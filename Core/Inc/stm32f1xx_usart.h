#ifndef STM32F1XX_USART_H
#define STM32F1XX_USART_H


#include "stm32f1xx.h"

#define USART_RXBUFF_SIZE 64            //max 256
#define USART_TXBUFF_SIZE 64            //max 256

#define USART_RXBUFF_MASK	(USART_RXBUFF_SIZE - 1)
#define USART_TXBUFF_MASK	(USART_TXBUFF_SIZE - 1)


void USART_init(void);
uint8_t USART_getc(void);
void USART_putc(uint8_t);
void USART_puts(char *s);
uint8_t USART_available(void);
void USART_flush(void);



#endif
